import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
import copy
from Configurables import *
from Configurables import DaVinci

from Configurables import TupleToolEventInfo,TupleToolDecay,TupleToolANNPID, TupleToolPid,TupleToolTISTOS, TupleToolTrigger, L0TriggerTisTos, TriggerTisTos
from Configurables import TupleToolEventInfo,TupleToolDecay,TupleToolANNPID, TupleToolPid, TupleToolTrigger ,  TupleToolPi0Info
from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo, BackgroundCategory
from Configurables import TupleToolSLTools , TupleToolIsoGeneric

from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict


triggerList = [
	"L0MuonDecision",
	#"Hlt1L0Decision",
	#"Hlt1TrackAllL0Decision",
	"Hlt1TrackMVADecision",
	#"Hlt1TwoTrackMVADecision", 
	"Hlt1TrackMVALooseDecision",
	#"Hlt1TwoTrackMVALooseDecision",
	"Hlt1TrackMuonMVADecision",
	#"Hlt1TrackMuonDecision",
	#"Hlt1SingleMuonHighPTDecision",
	#"Hlt1SingleMuonNoIPDecision",
	#"Hlt1DiMuonNoL0Decision",
	#"Hlt1DiMuonNoIPDecision",
	#"Hlt1DiMuonLowMassDecision",
        #"Hlt1DiMuonHighMassDecision",
	#"Hlt1MultiMuonNoL0Decision",
	#"Hlt1MultiMuonNoIPDecision",
	#"Hlt2SingleMuonDecision",
	#"Hlt2SingleMuonLowPTDecision",
	#"Hlt2SingleMuonHighPTDecision",
	#"Hlt2SingleMuonVHighPTDecision",
	"Hlt2XcMuXForTauB2XcMuDecision",
	"Hlt2TopoMu2BodyDecision",
	"Hlt2TopoMu3BodyDecision",
	"Hlt2TopoMu4BodyDecision",
	"Hlt2Topo2BodyDecision",
	"Hlt2Topo3BodyDecision",
	"Hlt2Topo4BodyDecision"]

mu_triggerList = [
	"L0MuonDecision",
	#"Hlt1L0Decision",
        #"Hlt1TrackAllL0Decision",
	"Hlt1TrackMVADecision",
	#"Hlt1TwoTrackMVADecision", 
	"Hlt1TrackMVALooseDecision",
	#"Hlt1TwoTrackMVALooseDecision",
	"Hlt1TrackMuonMVADecision",
	#"Hlt1TrackMuonDecision",
	#"Hlt1SingleMuonHighPTDecision",
	#"Hlt1SingleMuonNoIPDecision",
	# "Hlt1DiMuonNoL0Decision",
	# "Hlt1DiMuonNoIPDecision",
	# "Hlt1DiMuonLowMassDecision",
	# "Hlt1DiMuonHighMassDecision",
	# "Hlt1MultiMuonNoL0Decision",
	# "Hlt1MultiMuonNoIPDecision",
	"Hlt2XcMuXForTauB2XcMuDecision",
	"Hlt2SingleMuonDecision",
	"Hlt2SingleMuonLowPTDecision",
	"Hlt2SingleMuonHighPTDecision",
	"Hlt2SingleMuonVHighPTDecision"]

B_triggerList = [
	"Hlt2XcMuXForTauB2XcMuDecision",
	"Hlt2TopoMu2BodyDecision",
	"Hlt2TopoMu3BodyDecision",
	"Hlt2TopoMu4BodyDecision",
	"Hlt2Topo2BodyDecision",
	"Hlt2Topo3BodyDecision",
	"Hlt2Topo4BodyDecision"]

stable_variables = { "Q" : "Q",
		     "PX" : "PX",
		     "PY" : "PY",
		     "PZ" : "PZ",
		     "ETA": "ETA", 
		     "IP" : "BPVIP()",
		     "IPChi2" : "BPVIPCHI2()",
		     "ProbNNe" : "PROBNNe",
		     "ProbNNmu" : "PROBNNmu",
		     "ProbNNpi" : "PROBNNpi",
		     "ProbNNk" : "PROBNNk",
		     "ProbNNp" : "PROBNNp",
		     "PIDK" : "PIDK",
		     "PIDp" : "PIDp",
		     "PIDmu" : "PIDmu",
		     "ProbNNghost" : "PROBNNghost",
		     "IsMuon" : "switch(ISMUON,1,0)",
		     "HcalE"     : "PPINFO( LHCb.ProtoParticle.CaloHcalE , -10)" ,
		     "EcalE"     : "PPINFO( LHCb.ProtoParticle.CaloEcalE , -10)" ,
		     "PrsE"      : "PPINFO( LHCb.ProtoParticle.CaloPrsE , -10)"  ,
		     "VeloCharge": "PPINFO( LHCb.ProtoParticle.VeloCharge , -10)"
		     }

D_variables = { "M" : "MM", "PX" : "PX", "PY" : "PY", "PZ" : "PZ",
	        "DOCAMAX" : "DOCAMAX", "FDChi2" : "BPVVDCHI2",
	        "IP" : "BPVIP()", "IPChi2" : "BPVIPCHI2()", "VChi2" : "VFASPF(VCHI2/VDOF)",
	        "VX" : "VFASPF(VX)", "VY" : "VFASPF(VY)", "VZ" : "VFASPF(VZ)",
	        "PVX" : "BPV(VX)", "PVY" : "BPV(VY)", "PVZ" : "BPV(VZ)"
		}

Dstar_DTF_vars  = {
	"MASS_Dsi0Constr"       : "DTF_FUN ( M , True , strings ( ['D_s+','pi0'] ) )",
	"MASS_Dpi0Constr"       : "DTF_FUN ( M , True , strings ( ['D+'  ,'pi0'] ) )",
	"DTF_CHI2NDOF"          : "DTF_CHI2NDOF( True )",
	"DTF_CTAUERR"           : "DTF_CTAUERR( 0, True )",
	"MASS_DpConstr"         : "DTF_FUN ( M , True , 'D+' )" ,
	"MASS_DsConstr"         : "DTF_FUN ( M , True , 'D_s+' )" ,
	"DTF_VCHI2NDOF"         : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )"
	}
 

B_variables = { "M" : "MM", "PX" : "PX", "PY" : "PY", "PZ" : "PZ",
	        "DOCA" : "DOCA(1,2)", "FDChi2" : "BPVVDCHI2", "VChi2" : "VFASPF(VCHI2/VDOF)",
		"VX" : "VFASPF(VX)", "VY" : "VFASPF(VY)", "VZ" : "VFASPF(VZ)",
		"cosTheta1_star"   : "LV01", "cosTheta2_star"   : "LV02",
		"PVX" : "BPV(VX)", "PVY" : "BPV(VY)", "PVZ" : "BPV(VZ)" 
		}

DTF_var ={      "DTF_PX"            : "DTF_FUN(PX,False)",
                "DTF_PY"            : "DTF_FUN(PY,False)",
                "DTF_PZ"            : "DTF_FUN(PZ,False)",
                "DTF_M"             : "DTF_FUN(M, False)",
		"DTF_CHI2NDOF"      : "DTF_CHI2NDOF( True )",
		"ETA"               : "ETA",
		"PHI"               : "PHI",
		"DTF_TAU"           : "DTF_CTAU( 0, True )/0.299792458",
		"DTF_CTAUS"         : "DTF_CTAUSIGNIFICANCE( 0, True )",
		"DTF_D_PX"          : "DTF_FUN(CHILD(1,PX),False)",
                "DTF_D_PY"          : "DTF_FUN(CHILD(1,PY),False)",
                "DTF_D_PZ"          : "DTF_FUN(CHILD(1,PZ),False)",
                "DTF_D_M"           : "DTF_FUN(CHILD(1,M), False)",
                "DTF_K1_PX"         : "DTF_FUN(CHILD(1,CHILD(1,PX)),False)",
                "DTF_K1_PY"         : "DTF_FUN(CHILD(1,CHILD(1,PY)),False)",
                "DTF_K1_PZ"         : "DTF_FUN(CHILD(1,CHILD(1,PZ)),False)",
                "DTF_K1_M"          : "DTF_FUN(CHILD(1,CHILD(1,M)), False)",
                "DTF_K2_PX"         : "DTF_FUN(CHILD(1,CHILD(2,PX)),False)",
                "DTF_K2_PY"         : "DTF_FUN(CHILD(1,CHILD(2,PY)),False)",
                "DTF_K2_PZ"         : "DTF_FUN(CHILD(1,CHILD(2,PZ)),False)",
                "DTF_K2_M"          : "DTF_FUN(CHILD(1,CHILD(2,M)), False)",
                "DTF_Pi_PX"         : "DTF_FUN(CHILD(1,CHILD(3,PX)),False)",
                "DTF_Pi_PY"         : "DTF_FUN(CHILD(1,CHILD(3,PY)),False)",
                "DTF_Pi_PZ"         : "DTF_FUN(CHILD(1,CHILD(3,PZ)),False)",
                "DTF_Pi_M"          : "DTF_FUN(CHILD(1,CHILD(3,M)), False)",
                "DTF_Mu_PX"         : "DTF_FUN(CHILD(2,PX),False)",
                "DTF_Mu_PY"         : "DTF_FUN(CHILD(2,PY),False)",
                "DTF_Mu_PZ"         : "DTF_FUN(CHILD(2,PZ),False)",
                "DTF_Mu_M"          : "DTF_FUN(CHILD(2,M), False)"
		}

# Charged Isolation 
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence , MergedSelection
LongPions      = DataOnDemand( Location ='Phys/StdAllNoPIDsPions/Particles' )
UpStreamPions  = DataOnDemand( Location ='Phys/StdNoPIDsUpPions/Particles'  )
LongAndUpPions       = MergedSelection( "LongAndUpPions", RequiredSelections =[ LongPions , UpStreamPions ] )
LongAndUpPions_seq   = SelectionSequence('LongAndUpPions_seq', TopSelection = LongAndUpPions)
LongAndUpPions_seq.OutputLevel = FATAL
DaVinci().appendToMainSequence( [ LongAndUpPions_seq ])

def fillTuple( tuple, isMC = False ) :
	tuple.ToolList = [ "TupleToolPrimaries" , "TupleToolPi0Info"]
	tuple.OutputLevel = FATAL
	
	tt_trackpos = tuple.addTupleTool("TupleToolTrackPosition")
	tt_trackpos.OutputLevel = FATAL
	tt_trackpos.Z = 200.
	
	tt_recostats = tuple.addTupleTool("TupleToolRecoStats")
	tt_recostats.Verbose = True
	
	tt_geometry = tuple.addTupleTool("TupleToolGeometry")
	tt_geometry.Verbose = False
	tt_geometry.RefitPVs = False
	tt_geometry.FillMultiPV = False
	tt_geometry.OutputLevel = FATAL
	
	tt_trackinfo = tuple.addTupleTool("TupleToolTrackInfo") # 
	#tt_trackinfo.Verbose = True
	tt_trackinfo.OutputLevel = FATAL
	
	tt_eventinfo = tuple.addTupleTool("TupleToolEventInfo")
	tt_eventinfo.Verbose = False
	
	#tuple.addTool(TupleToolTrigger())
	#tuple.TupleToolTrigger.VerboseHlt2 = True
	#tuple.TupleToolTrigger.TriggerList = triggerList
	#tuple.TupleToolTrigger.VerboseL0 = True
	#tuple.TupleToolTrigger.VerboseHlt1 = True
	
	#tuple.addTool(TupleToolPid())
	#tuple.TupleToolPid.OutputLevel = 6 
	
	TupleToolIsoGeneric              =  tuple.addTupleTool("TupleToolIsoGeneric" )
	TupleToolIsoGeneric.ParticlePath =  LongAndUpPions_seq.outputLocation()
	#TupleToolIsoGeneric.VerboseMode  =  False
	
	LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso" )
	LoKiTool_noniso.Variables     = { "ETA" : "ETA" , "PHI" : "PHI" , "TRTYPE" : "TRTYPE"}
	TupleToolIsoGeneric.ToolList += ["LoKi::Hybrid::TupleTool/LoKiTool_noniso"]
	TupleToolIsoGeneric.addTool(LoKiTool_noniso)
	TupleToolIsoGeneric.OutputLevel = 6
	
	# Neutral isolation :
	'''
	from Configurables import TupleToolConeIsolation

	tuple.Mu.addTupleTool("TupleToolConeIsolation")
	tuple.Mu.TupleToolConeIsolation.MinConeSize       = 0.5
	tuple.Mu.TupleToolConeIsolation.SizeStep          = 0.5
	tuple.Mu.TupleToolConeIsolation.MaxConeSize       = 2.0
	tuple.Mu.TupleToolConeIsolation.OutputLevel = 6
	'''
	if isMC :
		from Configurables import MCMatchObjP2MCRelator
		default_rel_locs =MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
		rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]
		tuple.ToolList += [ "TupleToolMCTruth/MCTruth" ]
		tuple.addTool(TupleToolMCTruth("MCTruth"))
		tuple.MCTruth.addTool(MCMatchObjP2MCRelator)
		tuple.MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
		tuple.MCTruth.ToolList += [ "MCTupleToolKinematic", "MCTupleToolHierarchy" ] #, "MCTupleToolSemileptonic" ]
	tuple.addTool(TupleToolDecay, name="Mu")
        
	tuple.Mu.ToolList += [ "TupleToolTISTOS" ]
	tuple.Mu.addTool(TupleToolTISTOS())
	tuple.Mu.TupleToolTISTOS.addTool(L0TriggerTisTos())
	tuple.Mu.TupleToolTISTOS.addTool(TriggerTisTos())
	tuple.Mu.TupleToolTISTOS.TriggerList = mu_triggerList
	tuple.Mu.TupleToolTISTOS.Verbose = True
	tuple.Mu.TupleToolTISTOS.VerboseL0 = True
	tuple.Mu.TupleToolTISTOS.VerboseHlt1 = True

	
	MuLoKi = tuple.Mu.addTupleTool("LoKi::Hybrid::TupleTool/MuLoKi")
	MuLoKi.OutputLevel = 6
	MuLoKi.Variables = stable_variables
	
	tuple.addTool(TupleToolDecay, name="K1")
	K1LoKi = tuple.K1.addTupleTool("LoKi::Hybrid::TupleTool/K1LoKi")
	K1LoKi.OutputLevel = 6
	K1LoKi.Variables = stable_variables
	
	tuple.addTool(TupleToolDecay, name="K2")
	K2LoKi = tuple.K2.addTupleTool("LoKi::Hybrid::TupleTool/K2LoKi")
	K2LoKi.OutputLevel = 6
	K2LoKi.Variables = stable_variables
	
	tuple.addTool(TupleToolDecay, name="Pi")
	PiLoKi = tuple.Pi.addTupleTool("LoKi::Hybrid::TupleTool/PiLoKi")
	K2LoKi.OutputLevel = 6
	PiLoKi.Variables = stable_variables
	
	tuple.addTool(TupleToolDecay, name="Pi1_Dsstr")
	Pi1_DsstrLoKi = tuple.Pi1_Dsstr.addTupleTool("LoKi::Hybrid::TupleTool/Pi1_DsstrLoKi")
	Pi1_DsstrLoKi.OutputLevel = 6
	Pi1_DsstrLoKi.Variables = stable_variables	
	
	tuple.addTool(TupleToolDecay, name="Pi2_Dsstr")
	Pi2_DsstrLoKi = tuple.Pi2_Dsstr.addTupleTool("LoKi::Hybrid::TupleTool/Pi2_DsstrLoKi")
	Pi2_DsstrLoKi.OutputLevel = 6
	Pi2_DsstrLoKi.Variables = stable_variables

	
	tuple.addTool(TupleToolDecay, name="Pi0")
	Pi0_LoKi = tuple.Pi0.addTupleTool("LoKi::Hybrid::TupleTool/Pi0_LoKi")
	Pi0_LoKi.Variables = {
		"CL_gamma1" : "CHILD(CL,1)" , "CL_gamma2" : "CHILD(CL,2)" ,
		"PX_gamma1" : "CHILD(PX,1)" , "PX_gamma2" : "CHILD(PX,2)" ,
		"PY_gamma1" : "CHILD(PY,1)" , "PY_gamma2" : "CHILD(PY,2)" ,
		"PZ_gamma1" : "CHILD(PZ,1)" , "PZ_gamma2" : "CHILD(PZ,2)"
		#"HcalE"     : "PPINFO( LHCb.ProtoParticle.CaloHcalE , -10)" ,
		#"EcalE"     : "PPINFO( LHCb.ProtoParticle.CaloEcalE , -10)" ,
		#"PrsE"      : "PPINFO( LHCb.ProtoParticle.CaloPrsE , -10)"  ,
		}
	Pi0_LoKi.OutputLevel = 6
	
	tuple.addTool(TupleToolDecay, name="Dsstr")
	DsstrLoKi = tuple.Dsstr.addTupleTool("LoKi::Hybrid::TupleTool/DsstrLoKi")
	DsstrLoKi.OutputLevel = 6
	DsstrLoKi.Variables = dict(D_variables.items() + Dstar_DTF_vars.items())
	
	tuple.addTool(TupleToolDecay, name="D")
	DLoKi = tuple.D.addTupleTool("LoKi::Hybrid::TupleTool/DLoKi")
	DLoKi.Variables = D_variables
	DLoKi.OutputLevel = 6
	
	
	if isMC :
		tuple.D.ToolList += [ "TupleToolMCBackgroundInfo/DMCInfo" ]
		tuple.D.addTool(TupleToolMCBackgroundInfo("DMCInfo"))
		tuple.D.DMCInfo.addTool(BackgroundCategory())
		## DMCLoKi = tuple.D.addTupleTool("LoKi::Hybrid::TupleTool/DMCLoKi")
		## DMCLoKi.Preambulo.append("from LoKiPhysMC.decorators import *")
		## DMCLoKi.Preambulo.append("from LoKiPhysMC.functions import mcMatch")
		## DMCLoKi.Variables = {
		## 	"IsD2KKpi" : "switch ( mcMatch( '[(Meson & Charm) ==> K+ K- pi- ]CC' ), 1, 0)",
		## 	"IsDd2KKpi" : "switch ( mcMatch( '[(Meson & Charm & Down) ==> K+ K- pi- ]CC' ), 1, 0)",
		## 	"IsDs2KKpi" : "switch ( mcMatch( '[(Meson & Charm & Strange) ==> K+ K- pi- ]CC' ), 1, 0)" }
	
	tuple.addTool(TupleToolDecay, name="B")
	tuple.B.ToolList += [ "TupleToolTISTOS", "TupleToolSLTools" ]
	# tuple.B.ToolList += [  "TupleToolSLTools" ]
	print "SL Tools stuff happening. "
	tuple.B.addTool(TupleToolSLTools())
	tuple.B.TupleToolSLTools.Bmass = 5366.8 ## 5279.6 mass of B0 5366.8, mass of B0s
	tuple.B.addTool(TupleToolTISTOS())
	tuple.B.TupleToolTISTOS.addTool(TriggerTisTos())
	tuple.B.TupleToolTISTOS.TriggerList = B_triggerList
	tuple.B.TupleToolTISTOS.Verbose = True
	tuple.B.TupleToolTISTOS.VerboseHlt2 = True
	
	BLoKi = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/BLoKi")
	BLoKi.Variables = B_variables
	BLoKi.OutputLevel = 6
#this add to the tuple  B_DTF_D_PX, etc...
#DLoKi = tuple.B.addTupleTool("TupleToolDecayTreeFitter/DTF")
#tuple.B.DTF.UpdateDaughters = True;
         
	DictTuple =tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
	DictTuple.addTool(DTFDict,"DTF")
	DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
	DictTuple.NumVar = 20 
	DictTuple.OutputLevel = 6 
	DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
	DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
	DictTuple.DTF.dict.Variables = DTF_var
	
	
	if isMC :
		tuple.B.ToolList += [ "TupleToolMCBackgroundInfo/BMCInfo" ]
	#	tuple.B.ToolList += [ "TupleToolMCBackgroundInfo/BMCInfo", "TupleToolMCTruth/BTruth" ]
		tuple.B.addTool(TupleToolMCBackgroundInfo("BMCInfo"))
		tuple.B.BMCInfo.addTool(BackgroundCategory())
		tuple.B.BMCInfo.BackgroundCategory.InclusiveDecay=True
		tuple.B.BMCInfo.BackgroundCategory.SemileptonicDecay=True
		tuple.B.BMCInfo.BackgroundCategory.NumNeutrinos=3
	#	tuple.B.addTool(TupleToolMCTruth("BTruth"))
	#	tuple.B.BTruth.ToolList = []
	#	tuple.B.BTruth.addTool(MCTupleToolSemileptonic())
	## 	BMCLoKi = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/BMCLoKi")
	## 	BMCLoKi.Preambulo.append("from LoKiPhysMC.decorators import *")
	## 	BMCLoKi.Preambulo.append("from LoKiPhysMC.functions import mcMatch")
	## 	BMCLoKi.Variables = {
	## 		"IsBd2Dmunu" : "switch ( mcMatch( '[(Meson & Beauty & Down) => mu+ Nu ( (Meson & Charm & Down) ==> K+ K- pi- ) ]CC' ), 1, 0)	## 		"IsBs2Dmunu" : "switch ( mcMatch( '[(Meson & Beauty & Strange) => mu+ Nu ( (Meson & Charm & Strange) ==> K+ K- pi-) ]CC' ), 1, 0)",
	## 		"IsBd2Dtaunu" : "switch ( mcMatch( '[(Meson & Beauty & Down) => tau+ Nu ( (Meson & Charm & Down) ==> K+ K- pi- ) ]CC' ), 1, 0)",
	## 		"IsBs2Dtaunu" : "switch ( mcMatch( '[(Meson & Beauty & Strange) => tau+ Nu ( (Meson & Charm & Strange) ==> K+ K- pi-) ]CC' ), 1, 0)"}
	
#######################################################################

def fillTupleK2pi( tuple, isMC = False ) :   	
	tuple.ToolList = [ "TupleToolEventInfo", "TupleToolTrigger", "TupleToolPrimaries","TupleToolRecoStats","TupleToolPid" ]
#tuple.ToolList = [ "TupleToolEventInfo", "TupleToolTrigger", "TupleToolPrimaries","TupleToolRecoStats","TupleToolANNPID" ]
	#tuple.addTool(TupleToolPid())
#output level=1,2,3,4,5 the highest the most verbose information you have inside the tree
	#tuple.TupleToolPid.OutputLevel = 6
	tuple.addTool(TupleToolTrigger())
	tuple.TupleToolTrigger.TriggerList = triggerList
	tuple.TupleToolTrigger.VerboseL0 = True
	tuple.TupleToolTrigger.VerboseHlt1 = True
	tuple.TupleToolTrigger.VerboseHlt2 = True
	if isMC :
		from Configurables import MCMatchObjP2MCRelator
		default_rel_locs =MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
		rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]
		tuple.ToolList += [ "TupleToolMCTruth/MCTruth" ]
		tuple.addTool(TupleToolMCTruth("MCTruth"))
		tuple.MCTruth.addTool(MCMatchObjP2MCRelator)
		tuple.MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
		tuple.MCTruth.ToolList += [ "MCTupleToolKinematic", "MCTupleToolHierarchy" ]
		#tuple.MCTruth.ToolList += ["MCTupleToolSemileptonic"]
		# and add eventtuple for eff. estimation
		from Configurables import EventTuple , TupleToolEventInfo
		etuple = EventTuple()
		#DaVinci().UserAlgorithms  += [ etuple  ]
		
	tuple.addTool(TupleToolDecay, name="Mu")
	tuple.Mu.ToolList += [ "TupleToolTISTOS" ]
	tuple.Mu.addTool(TupleToolTISTOS())
	tuple.Mu.TupleToolTISTOS.addTool(L0TriggerTisTos())
	tuple.Mu.TupleToolTISTOS.addTool(TriggerTisTos())
	tuple.Mu.TupleToolTISTOS.TriggerList = mu_triggerList
	tuple.Mu.TupleToolTISTOS.Verbose = True
	tuple.Mu.TupleToolTISTOS.VerboseL0 = True
	tuple.Mu.TupleToolTISTOS.VerboseHlt1 = True
	MuLoKi = tuple.Mu.addTupleTool("LoKi::Hybrid::TupleTool/MuLoKi")
	MuLoKi.Variables = stable_variables

	tuple.addTool(TupleToolDecay, name="K")
	KLoKi = tuple.K.addTupleTool("LoKi::Hybrid::TupleTool/KLoKi")
	KLoKi.Variables = stable_variables

	tuple.addTool(TupleToolDecay, name="Pi1")
	Pi1LoKi = tuple.Pi1.addTupleTool("LoKi::Hybrid::TupleTool/Pi1LoKi")
	Pi1LoKi.Variables = stable_variables	

	tuple.addTool(TupleToolDecay, name="Pi2")
	Pi2LoKi = tuple.Pi2.addTupleTool("LoKi::Hybrid::TupleTool/Pi2LoKi")
	Pi2LoKi.Variables = stable_variables

	tuple.addTool(TupleToolDecay, name="D")
	DLoKi = tuple.D.addTupleTool("LoKi::Hybrid::TupleTool/DLoKi")
	DLoKi.Variables = D_variables

	if isMC :
		tuple.D.ToolList += [ "TupleToolMCBackgroundInfo/DMCInfo" ]
		tuple.D.addTool(TupleToolMCBackgroundInfo("DMCInfo"))
		tuple.D.DMCInfo.addTool(BackgroundCategory())
		DMCLoKi = tuple.D.addTupleTool("LoKi::Hybrid::TupleTool/DMCLoKi")
		DMCLoKi.Preambulo.append("from LoKiPhysMC.decorators import *")
		DMCLoKi.Preambulo.append("from LoKiPhysMC.functions import mcMatch")
		DMCLoKi.Variables = { "IsD2Kpipi" : "switch ( mcMatch( '[(Meson & Charm) ==> K+ pi- pi- ]CC' ), 1, 0)" }

	tuple.addTool(TupleToolDecay, name="B")
	##tuple.B.ToolList += [ "TupleToolTISTOS" ]
	tuple.B.ToolList += [ "TupleToolTISTOS", "TupleToolSLTools" ]
	##tuple.B.ToolList += [ "TupleToolSLTools" ]
	tuple.B.addTool(TupleToolSLTools())
	tuple.B.TupleToolSLTools.Bmass = 5279.6 #5366.8 ## 5279.6 mass of B0 5366.8, mass of B0s
	tuple.B.addTool(TupleToolTISTOS())
	tuple.B.TupleToolTISTOS.addTool(TriggerTisTos())
	tuple.B.TupleToolTISTOS.TriggerList = B_triggerList
	tuple.B.TupleToolTISTOS.Verbose = True
	tuple.B.TupleToolTISTOS.VerboseHlt2 = True
	BLoKi = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/BLoKi")
	BLoKi.Variables = B_variables
	DictTuple =tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
	DictTuple.addTool(DTFDict,"DTF")
	DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
	DictTuple.NumVar = 20 
	DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
	DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
	DictTuple.DTF.dict.Variables = DTF_var

	if isMC :
		tuple.B.ToolList += [ "TupleToolMCBackgroundInfo/BMCInfo" ]
		tuple.B.addTool(TupleToolMCBackgroundInfo("BMCInfo"))
		tuple.B.BMCInfo.addTool(BackgroundCategory())
		tuple.B.BMCInfo.BackgroundCategory.InclusiveDecay=True
		tuple.B.BMCInfo.BackgroundCategory.SemileptonicDecay=True
		tuple.B.BMCInfo.BackgroundCategory.NumNeutrinos=3
		## BMCLoKi = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/BMCLoKi")
		## BMCLoKi.Preambulo.append("from LoKiPhysMC.decorators import *")
		## BMCLoKi.Preambulo.append("from LoKiPhysMC.functions import mcMatch")
		## BMCLoKi.Variables = {
		## 	"IsBd2Dmunu" : "switch ( mcMatch( '[(Meson & Beauty & Down) => mu+ Nu ( (Meson & Charm & Down) ==> K+ pi- pi- ) ]CC' ), 1, 0)",
		## 	"IsBs2Dmunu" : "switch ( mcMatch( '[(Meson & Beauty & Strange) => mu+ Nu ( (Meson & Charm & Strange) ==> K+ pi- pi-) ]CC' ), 1, 0)",
		## 	"IsBd2Dtaunu" : "switch ( mcMatch( '[(Meson & Beauty & Down) => tau+ Nu ( (Meson & Charm & Down) ==> K+ pi- pi- ) ]CC' ), 1, 0)",
		## 	"IsBs2Dtaunu" : "switch ( mcMatch( '[(Meson & Beauty & Strange) => tau+ Nu ( (Meson & Charm & Strange) ==> K+ pi- pi-) ]CC' ), 1, 0)"}

#######################################################################

