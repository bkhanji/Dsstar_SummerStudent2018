import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import DecayTreeTuple, MCMatchObjP2MCRelator
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence , MergedSelection
from DecayTreeTuple.Configuration import *
from Configurables import FilterDesktop , CombineParticles
from Configurables import CondDB, DaVinci
import sys

os.getcwd()
sys.path.append(os.getcwd())

from B2DmuTupleTools import fillTuple

#######################################################################
# --- Begin MomentumCorrection ---
def MomentumCorrection():
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
## Apply the momentum smearing (for MC only)
    from Configurables import TrackSmearState as SMEAR
    smear = SMEAR('StateSmear')
    return smear
# ---  End MomentumCorrection  ---
DaVinci().UserAlgorithms += [MomentumCorrection()]
#######################################################################
#
# Define a DecayTreeTuple
#
# make Ds+ pi- pi+ peak
Unified_pion_cuts = "(MINTREE(ABSID=='pi+',PIDK)< 5.0 )& (MINTREE(ABSID=='pi+',PIDp)< 5.0 )& (MINTREE(ABSID=='pi+', PIDmu)< 5.0 ) & (P>2000.0) & (TRCHI2DOF < 3.0)& (TRGHOSTPROB < 0.35) & (PT>500 *MeV)& (MIPCHI2DV(PRIMARY)> 4.0) & (P> 3 *GeV)"

from GaudiConfUtils.ConfigurableGenerators import FilterInTrees
# Get Ds and Mu particles :
B_inputParts = AutomaticData(Location = "/Event/B2DMuNuXANDTAU/Phys/B2DMuNuX_Ds/Particles")
DsAlg = FilterInTrees(Code = "('D+'==ABSID)")
DsSel = Selection("FilterD",Algorithm = DsAlg,RequiredSelections = [B_inputParts])
MuAlg = FilterInTrees(Code = "('mu+'==ABSID)")
MuSel = Selection("FilterMu",Algorithm = MuAlg,RequiredSelections = [B_inputParts])
# GHet Pi+ and pi0 form the event :
LoosePions  = AutomaticData( Location ='Phys/StdLoosePions/Particles' )
PionsFltr   = FilterDesktop("PionsFltr" , Code = Unified_pion_cuts)
PionsSel    = Selection('PionsSel', Algorithm = PionsFltr , RequiredSelections = [LoosePions])
Ds1_particle    = CombineParticles('Ds1_particle' , DecayDescriptors = ['[ D_s1(2460)+ -> D+ pi+ pi-]cc'] ,
                                   CombinationCut   =   "(AM> 1900.0*MeV) & (AM<3500.0*MeV)" ,
                                   MotherCut        =   "(VFASPF(VCHI2/VDOF)< 25.0) & (PT>2000 *MeV)"
                                   )

#Ds1_particle.Preambulo     = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
# Make the selection for each case:
Ds1_Sel      = Selection("Ds1_Sel"     , Algorithm = Ds1_particle    , RequiredSelections =[DsSel, PionsSel ])
# Now re-make the B:
B2Ds1MuNu_particle = CombineParticles( 'B2Ds1MuNu_particle' ,
                                       DecayDescriptors = ['[B0 -> D_s1(2460)- mu+]cc','[B0 -> D_s1(2460)- mu-]cc'],
                                       CombinationCut   = "(AM> 2500.0*MeV) & (AM<8000.0*MeV)" ,
                                       MotherCut        = "(VFASPF(VCHI2/VDOF)< 25.0) & (PT> 3000)"
                                       )
B2Ds1MuNu_particle.Preambulo     = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
#B2Ds1MuNu_particle.MotherCut     = "(mcMatch (('[ [B_s~0]cc => D_s1(2460)+ mu- Neutrino ]CC')) )"
#B2Ds1MuNu_particle.MotherCut     = "(mcMatch (('[ [B_s~0]cc -> D*_s+ mu- Neutrino ]CC')) )"
#B2Ds1MuNu_particle.MotherCut     = "(mcMatch (('[ [B_s~0]cc ==> D_s+ mu- Neutrino ]CC')) )"
B2Ds1MuNu_particle.MotherCut      = "(mcMatch (('[ [B_s~0]cc --> D_s+ mu- ... ]CC')) )"
# make the corresponding selections for each case :
B_Sel   = Selection("B_Sel"   , Algorithm = B2Ds1MuNu_particle    , RequiredSelections= [ MuSel , Ds1_Sel      ] )
# Apply momentum scale in robust way :

# make the sequances for each case :
SeqBDs1Munu      = SelectionSequence( 'SeqBDs1Munu'     , TopSelection = B_Sel   )

tuple_Ds1           = DecayTreeTuple("Ds1Tuple")
tuple_Ds1.TupleName = "Ds1Tuple"
tuple_Ds1.Inputs    = [ SeqBDs1Munu.outputLocation() ]
tuple_Ds1.Decay     = '[ B0 -> ^( D_s1(2460)- -> ^(D- -> ^K+ ^K- ^pi-) ^pi+ ^pi- ) ^mu+ ]CC'
tuple_Ds1.addBranches({
    'B'         : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- ) mu+]CC',
    'Dsstr'     : '[B0 -> ^( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- ) mu+]CC',
    'D'         : '[B0 ->  ( D_s1(2460)- -> ^(D- ->  K+  K-  pi-)  pi+  pi- ) mu+]CC',
    'Mu'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- )^mu+]CC',
    'K1'        : '[B0 ->  ( D_s1(2460)- ->  (D- -> ^K+  K-  pi-)  pi+  pi- ) mu+]CC',
    'K2'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+ ^K-  pi-)  pi+  pi- ) mu+]CC',
    'Pi'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K- ^pi-)  pi+  pi- ) mu+]CC',
    'Pi1_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) ^pi+  pi- ) mu+]CC',
    'Pi2_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+ ^pi- ) mu+]CC',
    })
fillTuple(tuple_Ds1, True )
tuple_Ds1.OutputLevel = 6

seqBDsMuNuX_gaudiSeq      = GaudiSequencer('seqBDsMuNuX_gaudiSeq')             
seqBDs1MuNu_gaudiSeq      = GaudiSequencer('seqBDs1MuNu_gaudiSeq')     ;seqBDs1MuNu_gaudiSeq.OutputLevel=ERROR

DaVinci().UserAlgorithms += [ SeqBDs1Munu.sequence()     , tuple_Ds1      ]

DaVinci().InputType = 'DST'
DaVinci().TupleFile = "B2Dmudata_Ntuple.root"
DaVinci().PrintFreq = 10000

#from Configurables import CondDB                  
#CondDB(LatestGlobalTagByDataType =  '2016' )
#CondDB().UseLatestTags  =  ['2016']          

#from Configurables import DaVinciInit
#DaVinciInit().OutputLevel = ERROR
#MessageSvc().OutputLevel = ERROR

DaVinci().DataType = '2016'
DaVinci().Simulation = True
DaVinci().EvtMax = -1 #3000 #
# 15000
#DaVinci().EvtMax = 100
#information obtained by doing:
#lb-run DaVinci/v41r2 ipython -i ../../../scripts_option_davinci/lookinside_dst.py /eos/lhcb/user/m/mravonel/dst/00056175_00000001_3.AllStreams.dst'
#line = 'D2hhCompleteEventPromptDst2D2RSLine'
#advance(line)
#cands = evt['/Event/AllStreams/Phys/D2hhCompleteEventPromptDst2D2RSLine/Particles'];print cands[0]
#print evt['/Event/Rec/Header']

'''
from GaudiConf import IOHelper
IOHelper().inputFiles([
   '/eos/lhcb/user/b/bkhanji/DST/00068934_00000002_1.b2dmunuxandtau.dst'
   # '/eos/lhcb/user/b/bkhanji/DST/00069603_00000648_1.semileptonic.dst'
], clear=True)
'''
