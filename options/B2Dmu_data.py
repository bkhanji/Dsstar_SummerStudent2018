import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import DecayTreeTuple, MCMatchObjP2MCRelator
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence , MergedSelection
from DecayTreeTuple.Configuration import *
from Configurables import FilterDesktop , CombineParticles
from Configurables import CondDB, DaVinci
import sys

os.getcwd()
sys.path.append(os.getcwd())

from B2DmuTupleTools import fillTuple

#######################################################################
# --- Begin MomentumCorrection ---
def MomentumCorrection():
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
## Apply the momentum smearing (for MC only)
    from Configurables import TrackSmearState as SMEAR
    smear = SMEAR('StateSmear')
    return smear
# ---  End MomentumCorrection  ---
userAlgos = []
#userAlgos.append(MomentumCorrection())
#######################################################################
#
# Define a DecayTreeTuple
#
# make Ds+ pi- pi+ peak
Unified_pion_cuts = "(MINTREE(ABSID=='pi+',PIDK)< 5.0 )& (MINTREE(ABSID=='pi+',PIDp)< 5.0 )& (MINTREE(ABSID=='pi+', PIDmu)< 5.0 ) & (P>2000.0) & (TRCHI2DOF < 3.0)& (TRGHOSTPROB < 0.35) & (PT>500 *MeV)& (MIPCHI2DV(PRIMARY)> 4.0) & (P> 3 *GeV)"

from GaudiConfUtils.ConfigurableGenerators import FilterInTrees
# Get Ds and Mu particles :
from Configurables import TrackScaleState as SCALER
scaler = SCALER( 'Scaler' )#, RootInTES =  "/Event/Semileptonic")
DaVinci().UserAlgorithms += [ scaler ]


B_inputParts = AutomaticData(Location = "/Event/Semileptonic/Phys/B2DMuNuX_Ds/Particles")
DsAlg = FilterInTrees(Code = "('D+'==ABSID) & (PT>2000*MeV) & ( INTREE( ('K+'==ABSID) & (PIDK>5) & (PT>500) & (P>3000)))")
DsSel = Selection("FilterD",Algorithm = DsAlg,RequiredSelections = [B_inputParts])
MuAlg = FilterInTrees(Code = "('mu+'==ABSID) & (PIDmu > 5.0)")
MuSel = Selection("FilterMu",Algorithm = MuAlg,RequiredSelections = [B_inputParts])
# GHet Pi+ and pi0 form the event :
LoosePions  = AutomaticData( Location ='Phys/StdLoosePions/Particles' )
PionsFltr   = FilterDesktop("PionsFltr" , Code = Unified_pion_cuts)
PionsSel    = Selection('PionsSel', Algorithm = PionsFltr , RequiredSelections = [LoosePions])
Pi0_resolved= AutomaticData( Location ='Phys/StdLooseResolvedPi0/Particles' )
Pi0_merged  = AutomaticData( Location ='Phys/StdLooseMergedPi0/Particles' )
Pi0Fltr_m   = FilterDesktop("Pi0Fltr_m" , Code = '(PT>2000 *MeV)')
Pi0Fltr_r   = FilterDesktop("Pi0Fltr_r" , Code = '(CHILD(CL,1)>0.35) & (CHILD(CL,2)>0.35)')
Pi0Sel_m    = Selection('Pi0Sel_m', Algorithm =Pi0Fltr_m  , RequiredSelections = [Pi0_merged  ])
Pi0Sel_r    = Selection('Pi0Sel_r', Algorithm =Pi0Fltr_r  , RequiredSelections = [Pi0_resolved])
# Add two pions to Ds , pi0 to Ds :
Ks0_particle    = CombineParticles('Ks0_particle' , DecayDescriptors = ['KS0 -> pi+ pi-'] ,
                                   CombinationCut   =   "(AM> 250.0*MeV) & (AM<3500.0*MeV)" ,
                                   MotherCut        =   "(VFASPF(VCHI2/VDOF)< 10.0)"
                                   )
Ks0_sel         = Selection('Ks0_sel', Algorithm = Ks0_particle , RequiredSelections = [ PionsSel ])
Ds1_particle    = CombineParticles('Ds1_particle' , DecayDescriptors = ['[ D_s1(2460)+ -> D+ pi+ pi-]cc'] ,
                                   CombinationCut   =   "(AM> 1900.0*MeV) & (AM<4500.0*MeV)" ,
                                   MotherCut        =   "(VFASPF(VCHI2/VDOF)< 6.0) & (PT>2000 *MeV)"
                                   )
Dsstar_particle = CombineParticles('Dsstar_particle' ,
                                   DecayDescriptors = ['[ D*_s+ -> D+ pi0 ]cc'] ,
                                   CombinationCut   =   "(AM> 1900.0*MeV) & (AM<4500.0*MeV)" ,
                                   MotherCut        =   "(VFASPF(VCHI2/VDOF)< 6.0) & (PT>2000 *MeV)"
                                   )
Ds1Ks0_particle = CombineParticles('Ds1Ks0_particle' , DecayDescriptors = ['[ D_s1(2460)+ -> D+ KS0]cc'] ,
                                   CombinationCut   =   "(AM> 1900.0*MeV) & (AM<4500.0*MeV)" ,
                                   MotherCut        =   "(VFASPF(VCHI2/VDOF)< 6.0) & (PT>2000 *MeV)"
                                   )
# Make the selection for each case:
Ds1Ks0_Sel   = Selection("Ds1Ks0_Sel"  , Algorithm = Ds1Ks0_particle , RequiredSelections =[DsSel, Ks0_sel  ])
Ds1_Sel      = Selection("Ds1_Sel"     , Algorithm = Ds1_particle    , RequiredSelections =[DsSel, PionsSel ])
Dsstar_Sel_m = Selection("Dsstar_Sel_m", Algorithm = Dsstar_particle , RequiredSelections =[DsSel, Pi0Sel_m ])
Dsstar_Sel_r = Selection("Dsstar_Sel_r", Algorithm = Dsstar_particle , RequiredSelections =[DsSel, Pi0Sel_r ])

# Now re-make the B:
B2Ds1MuNu_particle = CombineParticles( 'B2Ds1MuNu_particle' ,
                                       DecayDescriptors = ['[B0 -> D_s1(2460)- mu+]cc','[B0 -> D_s1(2460)- mu-]cc'],
                                       CombinationCut   = "(AM> 2500.0*MeV) & (AM<8000.0*MeV)" ,
                                       MotherCut        = "(VFASPF(VCHI2/VDOF)< 9.0) & (PT> 3000)  & (BPVDIRA> 0.9995)"
                                       )
B2DsstarMuNu_particle = CombineParticles('B2DsstarMuNu_particle' ,
                                         DecayDescriptors = ['[B0 -> D*_s- mu+]cc', '[B0 -> D*_s- mu-]cc' ] ,
                                         CombinationCut   = "(AM> 2500.0*MeV) & (AM<8000.0*MeV)" ,
                                         MotherCut        = "(VFASPF(VCHI2/VDOF)< 9.0) & (PT> 3000) & (BPVDIRA> 0.9995)"
                                         )
# make the corresponding selections for each case :
B_Sel   = Selection("B_Sel"   , Algorithm = B2Ds1MuNu_particle    , RequiredSelections= [ MuSel , Ds1_Sel      ] )
BKs0_Sel= Selection("BKs0_Sel", Algorithm = B2Ds1MuNu_particle    , RequiredSelections= [ MuSel , Ds1Ks0_Sel   ] )
B_Sel_m = Selection("B_Sel_m" , Algorithm = B2DsstarMuNu_particle , RequiredSelections= [ MuSel , Dsstar_Sel_m ] )
B_Sel_r = Selection("B_Sel_r" , Algorithm = B2DsstarMuNu_particle , RequiredSelections= [ MuSel , Dsstar_Sel_r ] )
# Apply momentum scale in robust way :

# make the sequances for each case :
SeqBDs1Munu      = SelectionSequence( 'SeqBDs1Munu'     , TopSelection = B_Sel   )
SeqBDs1Ks0Munu   = SelectionSequence( 'SeqBDs1Ks0Munu'  , TopSelection = BKs0_Sel)
SeqBDsstrMunu_m  = SelectionSequence( 'SeqBDsstrMunu_m' , TopSelection = B_Sel_m )
SeqBDsstrMunu_r  = SelectionSequence( 'SeqBDsstrMunu_r' , TopSelection = B_Sel_r )

tuple_Ds1           = DecayTreeTuple("Ds1Tuple")
tuple_Ds1.TupleName = "Ds1Tuple"
tuple_Ds1.Inputs    = [ SeqBDs1Munu.outputLocation() ]
tuple_Ds1.Decay     = '[ B0 -> ^( D_s1(2460)- -> ^(D- -> ^K+ ^K- ^pi-) ^pi+ ^pi- ) ^mu+ ]CC'
tuple_Ds1.addBranches({
    'B'         : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- ) mu+]CC',
    'Dsstr'     : '[B0 -> ^( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- ) mu+]CC',
    'D'         : '[B0 ->  ( D_s1(2460)- -> ^(D- ->  K+  K-  pi-)  pi+  pi- ) mu+]CC',
    'Mu'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- )^mu+]CC',
    'K1'        : '[B0 ->  ( D_s1(2460)- ->  (D- -> ^K+  K-  pi-)  pi+  pi- ) mu+]CC',
    'K2'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+ ^K-  pi-)  pi+  pi- ) mu+]CC',
    'Pi'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K- ^pi-)  pi+  pi- ) mu+]CC',
    'Pi1_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) ^pi+  pi- ) mu+]CC',
    'Pi2_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+ ^pi- ) mu+]CC',
    })
fillTuple(tuple_Ds1, False )
tuple_Ds1.OutputLevel = 6

tuple_Ds1_SS        = tuple_Ds1.clone("Ds1Tuple_SS")
tuple_Ds1_SS.TupleName = "Ds1Tuple_SS"
tuple_Ds1_SS.Decay     = '[ B0 -> ^( D_s1(2460)- -> ^(D- -> ^K+ ^K- ^pi-) ^pi+ ^pi- ) ^mu- ]CC'
tuple_Ds1_SS.addBranches({
    'B'         : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- ) mu-]CC',
    'Dsstr'     : '[B0 -> ^( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- ) mu-]CC',
    'D'         : '[B0 ->  ( D_s1(2460)- -> ^(D- ->  K+  K-  pi-)  pi+  pi- ) mu-]CC',
    'Mu'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+  pi- )^mu-]CC',
    'K1'        : '[B0 ->  ( D_s1(2460)- ->  (D- -> ^K+  K-  pi-)  pi+  pi- ) mu-]CC',
    'K2'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+ ^K-  pi-)  pi+  pi- ) mu-]CC',
    'Pi'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K- ^pi-)  pi+  pi- ) mu-]CC',
    'Pi1_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) ^pi+  pi- ) mu-]CC',
    'Pi2_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)  pi+ ^pi- ) mu-]CC',
    })

tuple_Dsstar_m = tuple_Ds1.clone("DsstarTuple_m") #DecayTreeTuple("DsstarTuple_m")
tuple_Dsstar_m.TupleName = "DsstarTuple_m"
tuple_Dsstar_m.Inputs = [ SeqBDsstrMunu_m.outputLocation() ]
tuple_Dsstar_m.Decay = '[ B0 -> ^( D*_s- -> ^(D- -> ^K+ ^K- ^pi-) ^pi0 ) ^mu+]CC'
tuple_Dsstar_m.addBranches({
    'B'         : '[B0 ->  ( D*_s- ->  (D- ->  K+  K-  pi-)  pi0 ) mu+]CC',
    'D'         : '[B0 ->  ( D*_s- -> ^(D- ->  K+  K-  pi-)  pi0 ) mu+]CC',
    'Mu'        : '[B0 ->  ( D*_s- ->  (D- ->  K+  K-  pi-)  pi0 )^mu+]CC',
    'K1'        : '[B0 ->  ( D*_s- ->  (D- -> ^K+  K-  pi-)  pi0 ) mu+]CC',
    'K2'        : '[B0 ->  ( D*_s- ->  (D- ->  K+ ^K-  pi-)  pi0 ) mu+]CC',
    'Pi'        : '[B0 ->  ( D*_s- ->  (D- ->  K+  K- ^pi-)  pi0 ) mu+]CC',
    'Dsstr'     : '[B0 -> ^( D*_s- ->  (D- ->  K+  K-  pi-)  pi0 ) mu+]CC',
    'Pi0'       : '[B0 ->  ( D*_s- ->  (D- ->  K+  K-  pi-) ^pi0 ) mu+]CC'
    })
tuple_Dsstar_m.OutputLevel = 6

tuple_Dsstar_m_SS = tuple_Dsstar_m.clone("DsstarTuple_m_SS")
tuple_Dsstar_m_SS.TupleName = "DsstarTuple_m_SS"
tuple_Dsstar_m_SS.Decay = '[ B0 -> ^( D*_s- -> ^(D- -> ^K+ ^K- ^pi-) ^pi0 ) ^mu-]CC'
tuple_Dsstar_m_SS.addBranches({
    'B'         : '[B0 ->  ( D*_s- ->  (D- ->  K+  K-  pi-)  pi0 ) mu-]CC',
    'D'         : '[B0 ->  ( D*_s- -> ^(D- ->  K+  K-  pi-)  pi0 ) mu-]CC',
    'Mu'        : '[B0 ->  ( D*_s- ->  (D- ->  K+  K-  pi-)  pi0 )^mu-]CC',
    'K1'        : '[B0 ->  ( D*_s- ->  (D- -> ^K+  K-  pi-)  pi0 ) mu-]CC',
    'K2'        : '[B0 ->  ( D*_s- ->  (D- ->  K+ ^K-  pi-)  pi0 ) mu-]CC',
    'Pi'        : '[B0 ->  ( D*_s- ->  (D- ->  K+  K- ^pi-)  pi0 ) mu-]CC',
    'Dsstr'     : '[B0 -> ^( D*_s- ->  (D- ->  K+  K-  pi-)  pi0 ) mu-]CC',
    'Pi0'       : '[B0 ->  ( D*_s- ->  (D- ->  K+  K-  pi-) ^pi0 ) mu-]CC'
    })
tuple_Dsstar_m_SS.OutputLevel = 6

tuple_Dsstar_r = tuple_Dsstar_m.clone("DsstarTuple_r")
tuple_Dsstar_r.TupleName = "DsstarTuple_r"
tuple_Dsstar_r.Inputs = [ SeqBDsstrMunu_r.outputLocation() ]
tuple_Dsstar_r.OutputLevel = 6

tuple_Dsstar_r_SS = tuple_Dsstar_m_SS.clone("DsstarTuple_r_SS")
tuple_Dsstar_r_SS.TupleName = "DsstarTuple_r_SS"
tuple_Dsstar_r_SS.Inputs = [ SeqBDsstrMunu_r.outputLocation() ]
tuple_Dsstar_r_SS.OutputLevel = 6

tuple_Ds1Ks0           = tuple_Ds1.clone("Ds1Ks0Tuple")
tuple_Ds1Ks0.TupleName = "Ds1Ks0Tuple"
tuple_Ds1Ks0.Inputs    = [ SeqBDs1Ks0Munu.outputLocation() ]
tuple_Ds1Ks0.Decay     = '[ B0 -> ^( D_s1(2460)- -> ^(D- -> ^K+ ^K- ^pi-) ^(KS0 -> ^pi+ ^pi-) ) ^mu+ ]CC'
tuple_Ds1Ks0.addBranches({
    'B'         : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 ->  pi+  pi- )) mu+]CC',
    'Dsstr'     : '[B0 -> ^( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 ->  pi+  pi- )) mu+]CC',
    'D'         : '[B0 ->  ( D_s1(2460)- -> ^(D- ->  K+  K-  pi-) (KS0 ->  pi+  pi- )) mu+]CC',
    'Mu'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 ->  pi+  pi- ))^mu+]CC',
    'K1'        : '[B0 ->  ( D_s1(2460)- ->  (D- -> ^K+  K-  pi-) (KS0 ->  pi+  pi- )) mu+]CC',
    'K2'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+ ^K-  pi-) (KS0 ->  pi+  pi- )) mu+]CC',
    'Pi'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K- ^pi-) (KS0 ->  pi+  pi- )) mu+]CC',
    'KS0'       : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)^(KS0 ->  pi+  pi- )) mu+]CC',
    'Pi1_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 -> ^pi+  pi- )) mu+]CC',
    'Pi2_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 ->  pi+ ^pi- )) mu+]CC',
    })

tuple_Ds1Ks0_SS           = tuple_Dsstar_m_SS.clone("Ds1Ks0Tuple_SS")
tuple_Ds1Ks0_SS.TupleName = "Ds1Ks0Tuple_SS"
tuple_Ds1Ks0_SS.Inputs    = [ SeqBDs1Ks0Munu.outputLocation() ]
tuple_Ds1Ks0_SS.Decay     = '[ B0 -> ^( D_s1(2460)- -> ^(D- -> ^K+ ^K- ^pi-) ^(KS0 -> ^pi+ ^pi-) ) ^mu- ]CC'
tuple_Ds1Ks0_SS.addBranches({
    'B'         : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 ->  pi+  pi- )) mu-]CC',
    'Dsstr'     : '[B0 -> ^( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 ->  pi+  pi- )) mu-]CC',
    'D'         : '[B0 ->  ( D_s1(2460)- -> ^(D- ->  K+  K-  pi-) (KS0 ->  pi+  pi- )) mu-]CC',
    'Mu'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 ->  pi+  pi- ))^mu-]CC',
    'K1'        : '[B0 ->  ( D_s1(2460)- ->  (D- -> ^K+  K-  pi-) (KS0 ->  pi+  pi- )) mu-]CC',
    'K2'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+ ^K-  pi-) (KS0 ->  pi+  pi- )) mu-]CC',
    'Pi'        : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K- ^pi-) (KS0 ->  pi+  pi- )) mu-]CC',
    'KS0'       : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-)^(KS0 ->  pi+  pi- )) mu-]CC',
    'Pi1_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 -> ^pi+  pi- )) mu-]CC',
    'Pi2_Dsstr' : '[B0 ->  ( D_s1(2460)- ->  (D- ->  K+  K-  pi-) (KS0 ->  pi+ ^pi- )) mu-]CC',
    })

#######################################################################
seqBDsMuNuX_gaudiSeq      = GaudiSequencer('seqBDsMuNuX_gaudiSeq')             

seqBDs1MuNu_gaudiSeq      = GaudiSequencer('seqBDs1MuNu_gaudiSeq')     ;seqBDs1MuNu_gaudiSeq.OutputLevel=ERROR
seqBDs1Ks0MuNu_gaudiSeq   = GaudiSequencer('seqBDs1Ks0MuNu_gaudiSeq')  ;seqBDs1Ks0MuNu_gaudiSeq.OutputLevel=ERROR
seqBDsstarMuNu_m_gaudiSeq = GaudiSequencer('seqBDsstarMuNu_m_gaudiSeq');seqBDsstarMuNu_m_gaudiSeq.OutputLevel=ERROR
seqBDsstarMuNu_r_gaudiSeq = GaudiSequencer('seqBDsstarMuNu_r_gaudiSeq');seqBDsstarMuNu_r_gaudiSeq.OutputLevel=ERROR

seqBDs1MuNu_SS_gaudiSeq    =GaudiSequencer('seqBDs1MuNu_SS_gaudiSeq');seqBDs1MuNu_SS_gaudiSeq.OutputLevel=ERROR
seqBDs1Ks0MuNu_SS_gaudiSeq =GaudiSequencer('seqBDs1Ks0MuNu_SS_gaudiSeq');seqBDs1Ks0MuNu_SS_gaudiSeq.OutputLevel=ERROR
seqBDsstarMuNu_m_SS_gaudiSeq=GaudiSequencer('seqBDsstarMuNu_m_SS_gaudiSeq');seqBDsstarMuNu_m_SS_gaudiSeq.OutputLevel=ERROR
seqBDsstarMuNu_r_SS_gaudiSeq=GaudiSequencer('seqBDsstarMuNu_r_SS_gaudiSeq');seqBDsstarMuNu_r_SS_gaudiSeq.OutputLevel=ERROR

seqBDs1MuNu_gaudiSeq.Members      += [ SeqBDs1Munu.sequence()     , tuple_Ds1      ]
seqBDsstarMuNu_m_gaudiSeq.Members += [ SeqBDsstrMunu_m.sequence() , tuple_Dsstar_m ]
seqBDsstarMuNu_r_gaudiSeq.Members += [ SeqBDsstrMunu_r.sequence() , tuple_Dsstar_r ]

seqBDs1MuNu_SS_gaudiSeq.Members      += [ SeqBDs1Munu.sequence()     , tuple_Ds1_SS   ]
seqBDsstarMuNu_m_SS_gaudiSeq.Members += [ SeqBDsstrMunu_m.sequence() , tuple_Dsstar_m_SS ]
seqBDsstarMuNu_r_SS_gaudiSeq.Members += [ SeqBDsstrMunu_r.sequence() , tuple_Dsstar_r_SS ]

seqBDs1Ks0MuNu_SS_gaudiSeq.Members   += [ SeqBDs1Ks0Munu.sequence()  , tuple_Ds1Ks0_SS   ]
seqBDs1Ks0MuNu_gaudiSeq.Members   += [ SeqBDs1Ks0Munu.sequence()  , tuple_Ds1Ks0   ]

seqBDsMuNuX_gaudiSeq.Members      += [
    seqBDs1MuNu_gaudiSeq         ,
    #seqBDs1Ks0MuNu_gaudiSeq      ,
#   seqBDsstarMuNu_m_gaudiSeq    ,
#   seqBDsstarMuNu_r_gaudiSeq    ,
    seqBDs1MuNu_SS_gaudiSeq     , 
    #seqBDs1Ks0MuNu_SS_gaudiSeq   
#   seqBDsstarMuNu_m_SS_gaudiSeq ,
#   seqBDsstarMuNu_r_SS_gaudiSeq ,
    ]
seqBDsMuNuX_gaudiSeq.ModeOR        =  True
seqBDsMuNuX_gaudiSeq.ShortCircuit  =  False

DaVinci().UserAlgorithms += [ seqBDsMuNuX_gaudiSeq ]

DaVinci().InputType = 'DST'
DaVinci().TupleFile = "B2Dmudata_Ntuple.root"
DaVinci().PrintFreq = 10000

from Configurables import CondDB                  
CondDB(LatestGlobalTagByDataType =  '2016' )
#CondDB().UseLatestTags  =  ['2016']          

#from Configurables import DaVinciInit
#DaVinciInit().OutputLevel = ERROR
#MessageSvc().OutputLevel = ERROR

DaVinci().DataType = '2016'
DaVinci().Simulation = False
DaVinci().EvtMax = -1 #3000 #
# 15000
#DaVinci().EvtMax = 100
#information obtained by doing:
#lb-run DaVinci/v41r2 ipython -i ../../../scripts_option_davinci/lookinside_dst.py /eos/lhcb/user/m/mravonel/dst/00056175_00000001_3.AllStreams.dst'
#line = 'D2hhCompleteEventPromptDst2D2RSLine'
#advance(line)
#cands = evt['/Event/AllStreams/Phys/D2hhCompleteEventPromptDst2D2RSLine/Particles'];print cands[0]
#print evt['/Event/Rec/Header']

'''
from GaudiConf import IOHelper
IOHelper().inputFiles([
    #'/eos/lhcb/user/b/bkhanji/DST/00068934_00000002_1.b2dmunuxandtau.dst'
    '/afs/cern.ch/work/b/bkhanji/public/DST/00069603_00000648_1.semileptonic.dst'
], clear=True)
'''
